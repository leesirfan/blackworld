//
//  Quene.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-23.
//
//

#include "Quene.h"
USING_NS_CC;

Quene::~Quene(){
    CC_SAFE_RELEASE(_arrayObjs);
}

Quene::Quene(){
    _arrayObjs = CCArray::createWithCapacity(100);
    _arrayObjs->retain();
}

Quene * Quene::create(){
    Quene * quene = new Quene();
    quene->autorelease();
    return quene;
}

void Quene::push(cocos2d::CCObject * objToPush){
    _arrayObjs->addObject(objToPush);
}

void Quene::pop(){
    if (_arrayObjs->count()>0) {
        _arrayObjs->removeObjectAtIndex(0);
    }
}

int Quene::size(){
    return _arrayObjs->count();
}

CCObject * Quene::front(){
    if (_arrayObjs->count()>0) {
        return _arrayObjs->objectAtIndex(0);
    }
    return NULL;
}

CCArray * Quene::getArray(){
    return _arrayObjs;
}






