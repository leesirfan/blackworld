//
//  SocketClient.cpp
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-19.
//
//

#include "SocketClient.h"
#include "IOMessage.h"
#include "ThreadLock.h"
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
USING_NS_CC;

SocketClient::~SocketClient(){
    CC_SAFE_RELEASE(m_sendMsgQuene);
    CC_SAFE_RELEASE(m_recvMsgQuene);
}

SocketClient::SocketClient(std::string strIP,int portNum):m_cbRecvBuf(1024*60),
m_cbSendBuf(1024*60){
    
    m_iState = SocketClient_WAIT_CONNECT;
    
    m_iSocket=-1;
    _strIP=strIP;
    _iPort=portNum;
    
    m_sendMsgQuene = Quene::create();
    m_recvMsgQuene = Quene::create();
    m_sendMsgQuene->retain();
    m_recvMsgQuene->retain();
    
    pthread_mutex_init(&m_sendqueue_mutex, NULL);
    pthread_mutex_init(&m_thread_msg_mutex, NULL);
    pthread_cond_init(&m_thread_msg_cond, NULL);
    
    _bThreadSendCreated=false;
    _bThreadRecvCreated=false;
}

bool SocketClient::connectServer(){
    
    if (m_iSocket!=-1) {
        close(m_iSocket);
    }
    
    m_iSocket = socket(AF_INET, SOCK_STREAM, 0);
    
    if (m_iSocket==-1) {
        return false;
    }
    
    sockaddr_in socketAddress;
    memset(&socketAddress,0,sizeof(socketAddress));
    
    socketAddress.sin_family=AF_INET;
    socketAddress.sin_port=htons(_iPort);
    socketAddress.sin_addr.s_addr=inet_addr(_strIP.c_str());
    memset(&(socketAddress.sin_zero),0,sizeof(socketAddress.sin_zero));
    
    //try connecting...
    int iErrorCode=0;
	iErrorCode = connect(m_iSocket,(sockaddr*)&socketAddress,sizeof(socketAddress));
	if (iErrorCode==-1)
	{
		printf("socket connect error:%d\n",errno);
		return false;
	}
    
    if (!_bThreadRecvCreated) {
        //创建接收线程
        if (0 != pthread_create(&_pthread_t_recv, NULL, ThreadReceiveMessage, this)) {
            return false;
        }
        _bThreadRecvCreated=true;
    }
    
    //连接状态设置
    m_iState = SocketClient_OK;
    CCLog("connected to server  ... ");
    return true;
}

void SocketClient::startSendThread(){
    if (!_bThreadSendCreated) {
        pthread_create(&_pthread_t_send, NULL, ThreadSendMessage, this);
        _bThreadSendCreated=true;
    }
}

void * SocketClient::ThreadSendMessage(void *p){

    SocketClient * This = static_cast<SocketClient*>(p);
    
    //还没有连接server,且尝试连接失败时
    while (This->m_iState == SocketClient_WAIT_CONNECT &&
           !This->connectServer()) {
        //链接服务器1失败后，如果 服务器2 存在将连 服务器2 
    }
    
    ByteBuffer & sendBuff=This->m_cbSendBuf;
    int socket=This->m_iSocket;
    
    while (This->m_iState != SocketClient_DESTROY) {
        if (This->m_iState == SocketClient_OK) {
            //发送缓冲器有遗留的数据需要发送
            if (sendBuff.getPosition() > 0) {
                sendBuff.flip();
                int msgSRet = send(socket, sendBuff.getBuffer(), sendBuff.getLimit(), 0);
                if (msgSRet == -1) {
                    This->m_iState = SocketClient_DESTROY;
                    CCLog("发送数据失败,网络异常!");
                    return ((void *)0);
                }else{
                    
                }
                sendBuff.setPosition(sendBuff.getPosition()+msgSRet);
                sendBuff.compact();
            }
            
            //从消息队列中获取消息
            IOMessage * msg=NULL;
            while (This->m_iState != SocketClient_DESTROY
                   && This->m_sendMsgQuene->size() >0) {
                {
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    msg = (IOMessage *)This->m_sendMsgQuene->front();
                    This->m_sendMsgQuene->pop();
                }
                CCLog(" sendData length: %d  %ld" ,  msg->datalength(), sizeof(char));
            
                if (msg->datalength() + sendBuff.getPosition() > sendBuff.getLimit()) {
                    This->m_iState = SocketClient_DESTROY;
                    CCLog("send buffer is full, send thread stop!");
                    return ((void *)0);
                }
                
                sendBuff.put(msg->data, 0, msg->datalength());
                sendBuff.flip();

                int msgSRet = send(socket,(char *)sendBuff.getBuffer(),sendBuff.getLimit(),0);
                if (msgSRet == -1) {
                    This->m_iState = SocketClient_DESTROY;
                    CCLog("发送数据失败,网络异常!");
                    return ((void *)0);
                }
                
                sendBuff.setPosition(sendBuff.getPosition()+msgSRet);
                sendBuff.compact();
            }
        }
        
        if (This->m_iState != SocketClient_DESTROY &&
            This->m_sendMsgQuene->size() == 0) {
            //sleep
            struct timeval tv;
            struct timespec ts;
            gettimeofday(&tv, NULL);
            ts.tv_sec = tv.tv_sec+5;
            ts.tv_nsec = 0;
            
            ThreadLock lock(&This->m_thread_msg_mutex);
            pthread_cond_timedwait(&This->m_thread_msg_cond, &This->m_thread_msg_mutex, &ts);
        }
    }

    //根据实际情况进行发送
    return (void*)0;
}

bool g_bcheckReceivedMessage = true;
void * SocketClient::ThreadReceiveMessage(void *p){
    
    fd_set fdRead;
    
    struct timeval aTime;
    aTime.tv_sec=1;
    aTime.tv_usec=0;
    
    //最大多少秒,连接上收不到数据就提示用户,重新登录
    int maxIdleTimeInSeconds = 60*3;
    
    //最大多少秒，连接上收不到数据就提示用户，选择重连
    int hint2TimeInSeconds = 60;
    
    //多长时间没有收到任何数据,提示用户
    int hintTimeInSeconds = 30;
    
    struct timeval lastHintUserTime;
	struct timeval lastReceiveDataTime;
	struct timeval now;
    
    gettimeofday(&lastReceiveDataTime, NULL);
    lastHintUserTime = lastReceiveDataTime;
    
    SocketClient * This = static_cast<SocketClient*>(p);
    ByteBuffer * recvBuff = &This->m_cbRecvBuf;
    
    while (This->m_iState != SocketClient_DESTROY) {
        if (This->m_iState != SocketClient_OK) {
            usleep(1000);
            continue;
        }
        
        FD_ZERO(&fdRead);
        FD_SET(This->m_iSocket, &fdRead);
        
        aTime.tv_sec=1;
        aTime.tv_usec=0;
        
        int ret = select(This->m_iSocket+1,&fdRead,NULL,NULL,&aTime);
        if (ret == -1) {
            if (errno == EINTR) {
                printf("======   收到中断信号，什么都不处理＝＝＝＝＝＝＝＝＝");
            }else{
                This->m_iState = SocketClient_DESTROY;
                return ((void *)0);
            }
        }else if (ret == 0){
            gettimeofday(&now, NULL);
            if (g_bcheckReceivedMessage) {
                if (now.tv_sec - lastReceiveDataTime.tv_sec > maxIdleTimeInSeconds &&
                    lastHintUserTime.tv_sec > hintTimeInSeconds) {
                    
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    while (This->m_recvMsgQuene->size() > 0) {
                        This->m_recvMsgQuene->pop();
                    }
                    CCLog("您的网络已经出问题了！");
                }else if (now.tv_sec - lastReceiveDataTime.tv_sec > hint2TimeInSeconds &&
                          now.tv_sec - lastHintUserTime.tv_sec > hintTimeInSeconds){
                    lastHintUserTime=now;
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    CCLog("您的网络好像出问题了！");
                }else if (now.tv_sec - lastReceiveDataTime.tv_sec > hintTimeInSeconds &&
                          now.tv_sec - lastHintUserTime.tv_sec > hintTimeInSeconds){
                    lastHintUserTime=now;
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    CCLog("您的网络好像出问题了！");
                }
            }else{
                lastHintUserTime = now;
                lastReceiveDataTime = now;
            }
        }else if (ret>0){
            if (FD_ISSET(This->m_iSocket, &fdRead)) {
                CCLog(" recvbuffer remaining %d \n", recvBuff->remaining());
                int iRetCode = 0;
                if (recvBuff->remaining() > 0) {
                    //接收数据到recvBuffer中
                    iRetCode = recv(This->m_iSocket, recvBuff->getBuffer()+recvBuff->getPosition(), recvBuff->remaining(), 0);
                }
                CCLog(" recv data length %d \n", iRetCode);
                
                if (iRetCode == -1) {
                    This->m_iState = SocketClient_DESTROY;
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    
                    while (This->m_recvMsgQuene->size()>0) {
                        This->m_recvMsgQuene->pop();
                    }
                    CCLog("网络连接中断！");
                    return (void*)0;
                }else if (iRetCode == 0 &&          //服务器对端的接口已经关闭
                          recvBuff->remaining() > 0){
                    This->m_iState = SocketClient_DESTROY;
                    ThreadLock lock(&This->m_sendqueue_mutex);
                    
                    while (This->m_recvMsgQuene->size()>0) {
                        This->m_recvMsgQuene->pop();
                    }
                    CCLog("服务器主动关闭连接!");
                    return (void*)0;
                }else{
                    //成功接收
                    gettimeofday(&lastReceiveDataTime, NULL);
                    recvBuff->setPosition(recvBuff->getPosition() + iRetCode);
                    recvBuff->flip();
                    
                    int tmpOffset = 17;
                    while(recvBuff->remaining() > tmpOffset){
                        int pos = recvBuff->position;
						int length= recvBuff->getLength(9);
                        if(recvBuff->remaining()+tmpOffset >= length){
                            IOMessage * recvMsg=IOMessage::create();
                            recvMsg->HEAD0 = recvBuff->getByte();
                            recvMsg->HEAD1 = recvBuff->getByte();
                            recvMsg->HEAD2 = recvBuff->getByte();
                            recvMsg->HEAD3 = recvBuff->getByte();
                            recvMsg->ProtoVersion = recvBuff->getByte();
                            recvBuff->getAsBytes(recvMsg->serverVersion);
                            recvBuff->getAsBytes(recvMsg->length);
                            recvBuff->getAsBytes(recvMsg->commandId);
                            
                            printf("message length: %d commandId: %d \n", ByteBuffer::bytesToInt(recvMsg->length),ByteBuffer::bytesToInt(recvMsg->commandId));
                            
                            char* tmp = new char[length-3];
							recvBuff->get(tmp,0,length-4);
                            tmp[length-4] = '\0';
                            recvMsg->data = tmp;
                            printf("%s",tmp);
                            
                            ThreadLock lock(&This->m_sendqueue_mutex);
                            This->m_recvMsgQuene->push(recvMsg);
                        }else if(length>recvBuff->getCapacity()){
							This->m_iState = SocketClient_DESTROY;
                            
							ThreadLock lock(&This->m_sendqueue_mutex);
							while( This->m_recvMsgQuene->size()>0){
								This->m_recvMsgQuene->pop();
							}
							
							CCLog("数据包太大，中断接收");
							return ((void *)0);
						}else {
							//printf("----------------------------\n");
							recvBuff->position = pos;
							break;
						}
                    }
                    recvBuff->compact();
                }
            }
        }
    }
    return (void*)0;
}

IOMessage * SocketClient::constructMessage(const char * data,int commandId){
    IOMessage * msgConstructed = IOMessage::create();
    
    msgConstructed->HEAD0 = 78;
    msgConstructed->HEAD1 = 37;
    msgConstructed->HEAD2 = 38;
    msgConstructed->HEAD3 = 48;
    
    msgConstructed->ProtoVersion = 9;
    
    int a=0;
    msgConstructed->serverVersion[3]=(unsigned char)(0xff&a);;
    msgConstructed->serverVersion[2]=(unsigned char)((0xff00&a)>>8);
    msgConstructed->serverVersion[1]=(unsigned char)((0xff0000&a)>>16);
    msgConstructed->serverVersion[0]=(unsigned char)((0xff000000&a)>>24);
    
    int b=strlen(data)+4;
    msgConstructed->length[3]=(unsigned char)(0xff&b);
    msgConstructed->length[2]=(unsigned char)((0xff00&b)>>8);
    msgConstructed->length[1]=(unsigned char)((0xff0000&b)>>16);
    msgConstructed->length[0]=(unsigned char)((0xff000000&b)>>24);
    
    int c=commandId;
    msgConstructed->commandId[3]=(unsigned char)(0xff&c);
    msgConstructed->commandId[2]=(unsigned char)((0xff00&c)>>8);
    msgConstructed->commandId[1]=(unsigned char)((0xff0000&c)>>16);
    msgConstructed->commandId[0]=(unsigned char)((0xff000000&c)>>24);
    
    CCLog("%d" ,msgConstructed->datalength());
    msgConstructed->data = new char[msgConstructed->datalength()];
    memcpy(msgConstructed->data+0,&msgConstructed->HEAD0,1);
    memcpy(msgConstructed->data+1,&msgConstructed->HEAD1,1);
    memcpy(msgConstructed->data+2,&msgConstructed->HEAD2,1);
    memcpy(msgConstructed->data+3,&msgConstructed->HEAD3,1);
    memcpy(msgConstructed->data+4,&msgConstructed->ProtoVersion,1);
    memcpy(msgConstructed->data+5,&msgConstructed->serverVersion,4);
    memcpy(msgConstructed->data+9,&msgConstructed->length,4);
    memcpy(msgConstructed->data+13,&msgConstructed->commandId,4);
    memcpy(msgConstructed->data+17,data,strlen(data));

    return msgConstructed;
}

void SocketClient::sendMessage(IOMessage * msgSend){
    if (m_iState == SocketClient_DESTROY) {
        return;
    }
    
    {
        //发送队列同步锁
        ThreadLock lock(&m_sendqueue_mutex);
        //添加到发送队列
        m_sendMsgQuene->push(msgSend);
    }
    
    if (m_iState == SocketClient_OK) {
        ThreadLock lock(&m_thread_msg_mutex);
        pthread_cond_signal(&m_thread_msg_cond);
    }
}











