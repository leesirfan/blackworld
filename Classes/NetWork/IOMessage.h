//
//  IOMessage.h
//  BlackDuck
//
//  Created by huxiaozhou on 14-2-22.
//
//

#ifndef __BlackDuck__IOMessage__
#define __BlackDuck__IOMessage__

#include <iostream>
#include "cocos2d.h"

class IOMessage:public cocos2d::CCObject {
private:

public:
    char HEAD0;
    char HEAD1;
    char HEAD2;
    char HEAD3;
    char ProtoVersion;
    
    unsigned char serverVersion[4];
    unsigned char length[4];
    unsigned char commandId[4];
    /**
     * 消息的数据
     */
    char * data;
    
public:
    ~IOMessage();
    IOMessage();
    static IOMessage * create();
    int datalength();
    int getCommondId();
    char * getData();
};

#endif /* defined(__BlackDuck__IOMessage__) */
