require "CCBReaderLoad"
require "lua/utils/utils"

LayerLogin = LayerLogin or {}
ccb["LayerLogin"] = LayerLogin

local editName
local editPwd

function loginLayer()
    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_login.ccbi",proxy,LayerLogin)
    local  layer = tolua.cast(node,"CCLayer")
    
    
    local nodeUserName = tolua.cast(LayerLogin["userName"],"CCNode")    
    local nodeUserPwd = tolua.cast(LayerLogin["userPwd"],"CCNode")  
    
    local nX,nY = nodeUserName:getPosition()
    local pX,pY = nodeUserPwd:getPosition()

    editName = CCEditBox:create(CCSizeMake(280,40), CCScale9Sprite:create("extensions/green_edit.png"))
    editName:setPosition(ccp(nX,nY))
        
    editPwd = CCEditBox:create(CCSizeMake(280,40), CCScale9Sprite:create("extensions/green_edit.png"))
    editPwd:setPosition(ccp(pX,pY))
    
    local loginPanel = tolua.cast(LayerLogin["loginPanel"],"CCSprite")    
    if nil ~= loginPanel then
        loginPanel:addChild(editName)
        loginPanel:addChild(editPwd)
    end

    return layer
end

local function onCCControlButtonLoginClicked()
    local userName = editName:getText();
    local userPwd = editPwd:getText();
    cclog(userName);
    cclog(userPwd);
    
    
    -------用户信息-------
    local tabLoginInfo ={}
    tabLoginInfo["username"] = userName
    tabLoginInfo["password"] = userPwd
    
    --构造json
    local jsonData = cjson.encode(tabLoginInfo)
    
end

LayerLogin["onCCControlButtonLoginClicked"] = onCCControlButtonLoginClicked
