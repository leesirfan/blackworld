LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dlua_shared

LOCAL_MODULE_FILENAME := libcocos2dlua

LOCAL_SRC_FILES := hellolua/main.cpp \
../../Classes/AppDelegate.cpp \
../../Classes/NetWork/SocketClient.cpp \
../../Classes/NetWork/ByteBuffer.cpp \


LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/NetWork
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../external/WxSqlite3/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../external/protobuf/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../external/jsonbox/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../cocos2dx//support/tinyxml2
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../external/lua-cjson
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../external/protobuf-lua

LOCAL_STATIC_LIBRARIES := curl_static_prebuilt


LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_lua_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_WHOLE_STATIC_LIBRARIES += jsonbox_static
LOCAL_WHOLE_STATIC_LIBRARIES += protobuf_static
LOCAL_WHOLE_STATIC_LIBRARIES += wxsqlite3_static
LOCAL_WHOLE_STATIC_LIBRARIES += lua_cjson_static
LOCAL_WHOLE_STATIC_LIBRARIES += lua_protobuf_static


include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,CocosDenshion/android)
$(call import-module,scripting/lua/proj.android)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
$(call import-module,external/WxSqlite3)
$(call import-module,external/protobuf)
$(call import-module,external/jsonbox)
$(call import-module,external/lua-cjson)
$(call import-module,external/protobuf-lua)