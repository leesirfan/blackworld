function FangKuaiManager:panDuanGameEnd()
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			--判断是否是空方块
			if fk.fenShu == 0 then
				--cclog("我是方块%d,%d,我是空的,游戏继续",fk.X,fk.Y)
				return true
			end

			--判断下方是否可以融合
			if i > 1 then
				local fk1 = fangKuaiManager.fangKuaiTable[i-1][j]
				if fk.fenShu+fk1.fenShu == 3 and fk.fenShu~= 3 or fk.fenShu >= 3 and fk.fenShu == fk1.fenShu then
					--cclog("我是方块%d,%d,我下方可以融合,游戏继续",fk.X,fk.Y)
					return true
				end
			end

			--判断上方是否可以融合
			if i < HANG then
				local fk1 = fangKuaiManager.fangKuaiTable[i+1][j]
				if fk.fenShu+fk1.fenShu == 3 and fk.fenShu~= 3 or fk.fenShu >= 3 and fk.fenShu == fk1.fenShu then
					--cclog("我是方块%d,%d,我上方可以融合,游戏继续",fk.X,fk.Y)
					return true
				end
			end

			--判断左方是否可以融合
			if j > 1 then
				local fk1 = fangKuaiManager.fangKuaiTable[i][j-1]
				if fk.fenShu+fk1.fenShu == 3 and fk.fenShu~= 3 or fk.fenShu >= 3 and fk.fenShu == fk1.fenShu then
					--cclog("我是方块%d,%d,我左方可以融合,游戏继续",fk.X,fk.Y)
					return true
				end
			end

			--判断右方是否可以融合
			if j < LIE then
				local fk1 = fangKuaiManager.fangKuaiTable[i][j+1]
				if fk.fenShu+fk1.fenShu == 3 and fk.fenShu~= 3 or fk.fenShu >= 3 and fk.fenShu == fk1.fenShu then
					--cclog("我是方块%d,%d,我右方可以融合,游戏继续",fk.X,fk.Y)
					return true
				end
			end
		end
	end

	return false
end

--终结游戏的函数
function FangKuaiManager:gameEnd()

	GV_showGameOverAd()
	--呼出结算
	gameLayer.XSShuoMing:setVisible(false)
	gameLayer.mAnimationManager:runAnimationsForSequenceNamed("jieSuanLai")
end

function GameLayer:jieSuanLaiFunC()
	gameManager.jiaoDianCeng = "jieSuanIng"
	--当前游戏局截图
	local jieTu = CCRenderTexture:create(kuangSize.width,kuangSize.height)
	local kuangPosX,kuangPosY = gameLayer.kuang:getPosition()
	gameLayer.kuang:setPosition(kuangSize.width*0.5,kuangSize.height*0.5)
	jieTu:begin()
	gameLayer.kuangZeroNode:visit()
	jieTu:endToLua()
	local random = math.ceil(math.random() * 99999 + 1)
	fangKuaiManager.jieTuName = random..".png"
    jieTu:saveToFile(CCFileUtils:sharedFileUtils():getWritablePath()..fangKuaiManager.jieTuName)
	gameLayer.kuang:setPosition(kuangPosX, kuangPosY)

	local xiaoChu = {}
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			table.insert(xiaoChu,fk)
		end
	end
	local function sortFunc (a, b)
	    return a.fenShu < b.fenShu
	end
	table.sort(xiaoChu,sortFunc)


	for i=1,table.getn(xiaoChu) do
		
		local fk = xiaoChu[i]
		local function funC()
			if i == 4 then
				self:gameEndAD()
			end
			fk.mAnimationManager:runAnimationsForSequenceNamed("jieSuan")
			fk.sp:setPosition(fk.beginPos)
		end
		GF_performWithDelay(fangKuaiManager.endGameFangKuaiYanChi,funC)
		fangKuaiManager.endGameFangKuaiYanChi = fangKuaiManager.endGameFangKuaiYanChi + ENDGAMEFANGKUAIYANSHI
	end

	--游戏结束,保存本局游戏
	local function funC2()
		CCDirector:sharedDirector():getScheduler():setTimeScale(1)
		gameManager.jiaoDianCeng = "NameLayer"
	end
	GF_performWithDelay(fangKuaiManager.endGameFangKuaiYanChi,funC2)
end

function GameLayer:gameEndAD()
	cclog("没有可以融合的方块了,游戏结束")
	SimpleAudioEngine:sharedEngine():playBackgroundMusic("Java_GameOver",true);
end