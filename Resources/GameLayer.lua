GameLayer=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )

function GameLayer:createLayer()
	cclog("游戏主场景 - 被创建")
	self.layer = CCBuilderReaderLoad("GameLayer.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0,winSize.height))
	gameLayer = self
	scene:addChild(self.layer)

	--初始化游戏场景
  	self:init()
end

function GameLayer:deleteSelf()
	cclog("游戏主场景 - 被删除")
	gameLayer.layer:removeFromParentAndCleanup(true)
	gameLayer = nil
end

function GameLayer:init()
    --初始化方块的管理者
    FangKuaiManager():new()
    --更新管理员参数
    gameManager.isGameing = true

    --计算游戏屏幕坐标的偏移量
	self.zeroNode:setPosition(-winSize.width*0.5,-winSize.height*0.5)
	pianYiPos = self.zeroNode:convertToWorldSpaceAR(ccp(0,0))
	--cclog("当前屏幕的左下角坐标为%d,%d",pianYiPos.x,pianYiPos.y)
    	
	--计算出左下角(0,0)的坐标点
	kuangSize = self.kuang:getContentSize()
    OriginX = kuangSize.width*0.5 - ( LIE + 1 ) * spSize.width * 0.5
    OriginY = kuangSize.height*0.5 - ( HANG + 1 ) * spSize.height * 0.5

	--注册点击事件
    self.layer:unregisterScriptTouchHandler()
    self.layer:registerScriptTouchHandler(onGameLayerTouch,false,-1,false)

    --判断游戏是否是新手模式
    if isXSMoShi then
        --启动新手模式
        XinShouManager():new()
    else
        --初始化游戏方框
        fangKuaiManager:suiJiChuShiHuaFangKuai()
        --生成下一个方块
        fangKuaiManager:nextFangKuaiRandom()
    end
end

--按钮的回调函数,进入游戏
function GameLayer:gameOut()
    playEffect("whiff.wav")
    BeginLayer():createLayer()
    gameManager.jiaoDianCeng = "BeginLayer"
    gameLayer.layer:setTouchEnabled(true)
    --根据游戏是否正在进行中,控制开始游戏按钮的图案
    beginLayer.kaishi:setBackgroundSpriteFrameForState((gameManager.isGameing and  CCSprite:create("back.png"):displayFrame()) or  CCSprite:create("retry.png"):displayFrame(),CCControlStateNormal)
    beginLayer.kaishi:setBackgroundSpriteFrameForState((gameManager.isGameing and  CCSprite:create("back.png"):displayFrame()) or  CCSprite:create("retry.png"):displayFrame(),CCControlStateHighlighted)
    beginLayer.kaishi:setBackgroundSpriteFrameForState((gameManager.isGameing and  CCSprite:create("back.png"):displayFrame()) or  CCSprite:create("retry.png"):displayFrame(),CCControlStateDisabled)
    beginLayer.startTTF:setString((gameManager.isGameing and "back") or "retry")
    beginLayer:butOffOn(false)
    gameLayer:butOffOn(false)
    local function funC()
        if gameManager.isGameing == false then
            BEGINLAYERTINGZHIKONGJIAN = 3
            self:deleteSelf() 
        else
            self.layer:setPosition(ccp(0,winSize.height))
        end
    end
    --播放离开动画
    local move = CCEaseExponentialOut:create(CCMoveBy:create(TIAOZHUANCHANGJINGTIME, ccp(0,winSize.height)))
    self.layer:runAction(move)
    GF_performWithDelay(TIAOZHUANCHANGJINGTIME,funC)

    --播放BeginLayer入场动画
    beginLayer:layerIn()
end

function GameLayer:butOffOn(is)
    gameLayer.menuBu:setEnabled(is)
end