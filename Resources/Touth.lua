function onGameLayerTouch(eventType, x, y)
	if gameManager.jiaoDianCeng == "GameLayer" then
	    if eventType == "began" then
	        return onGameLayerTouchBegan(x, y)
	    end
	    if eventType == "moved" then
	        return onGameLayerTouchMoved(x, y)
	    end
	    if eventType == "ended" then
	        return onGameLayerTouchEnded(x, y)
	    end
	end

	if gameManager.jiaoDianCeng == "NameLayer" then
	    if eventType == "began" then
	        return namelayerTouth(x, y)
	    end
	end

	if gameManager.jiaoDianCeng == "jieSuanIng" then
	    if eventType == "began" then
	        return jieSuanIngTouth(x, y)
	    end
	end

	if gameManager.jiaoDianCeng == "HightFenTanKuang" then
	    if eventType == "began" then
	        return hightFenTanKuangTouthBegan(x, y)
	    end
	    if eventType == "moved" then
	        return hightFenTanKuangTouthMoved(x, y)
	    end
	    if eventType == "ended" then
	        return hightFenTanKuangTouthEnded(x, y)
	    end
	end
end

function hightFenTanKuangTouthBegan(x, y)
	BeginPos = {x = x, y = y}
    --保存当前的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)
    return true
end

function hightFenTanKuangTouthMoved(x, y)
	--根据当前的点击位置与上一帧对比,获得移动的距离
	moveChangDu = ccp(moveLastPos.x-x,moveLastPos.y-y)
	--保存当前帧的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)
	--获取移动移动的距离
	local posX,posY = hightFenTanKuang.hightFen:getPosition()
	local movePos = {x = BeginPos.x - x , y = BeginPos.y - y }
	hightFenTanKuang.hightFen:setPosition(ccp(posX -  moveChangDu.x ,posY))
    return true
end



function hightFenTanKuangTouthEnded(x, y)
	local posX,posY = hightFenTanKuang.hightFen:getPosition()
	if math.abs( posX ) > 100 then
		if posX > 0 then
			hightFenTanKuang.fangxiang = "left"
		else
			hightFenTanKuang.fangxiang = "right"
		end
		hightFenTanKuang:deleteSelf()
	end
    return true
end

function jieSuanIngTouth(x, y)
	if fangKuaiManager.gameEndFanPaiSpeed < ENDGAMECLICKJIASUSHANGXIAN then
		fangKuaiManager.gameEndFanPaiSpeed = fangKuaiManager.gameEndFanPaiSpeed + ENDGAMECLICKJIASU
    	CCDirector:sharedDirector():getScheduler():setTimeScale(fangKuaiManager.gameEndFanPaiSpeed)
    end
    return false
end

function namelayerTouth(x, y)
    NameLayer():createLayer()
    gameManager.jiaoDianCeng = ""
    return false
end

function onGameLayerTouchBegan(x, y)
    BeginPos = {x = x, y = y}
    --保存当前的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)

	--判断游戏是否已经执行过结算
	if fangKuaiManager.isJieSuan == false then
		--没有执行过结算,判断游戏是否结束
		local isGameEnd = fangKuaiManager:panDuanGameEnd()
		if isGameEnd  == false then
			--已经结束
			fangKuaiManager.isJieSuan = true
			gameManager.jiaoDianCeng = ""
			fangKuaiManager:gameEnd()
			--记录本局游戏
			FangKuaiManager:jiLuShuJu()
			return false
		end
	end
    return true
end

function onGameLayerTouchMoved(x, y)
	--根据当前的点击位置与上一帧对比,获得移动的距离
	moveChangDu = ccp(moveLastPos.x-x,moveLastPos.y-y)
	--保存当前帧的坐标,下一帧判断使用
	moveLastPos = ccp(x,y)
	--获取移动移动的距离
	local movePos = {x = BeginPos.x - x , y = BeginPos.y - y }
	--判断是否已经判断出移动方向
	if fangKuaiManager.isMoveing then
		--方块移动
		fangKuaiManager:moveing()
	else
		--判断是否移动到安全距离
		if math.abs( movePos.x ) > 20 or math.abs( movePos.y ) > 20 then
			--已经判断出方向,设置状态
			fangKuaiManager.isMoveing = true
			--判断方向
			if math.abs( movePos.x ) > math.abs( movePos.y ) then
				if movePos.x > 0 then
					--向左移动
					fangKuaiManager:moveLeft()
				else
					--向右移动
					fangKuaiManager:moveRight()
				end
			else
				if movePos.y > 0 then
					--向下移动
					fangKuaiManager:moveXia()
				else
					--向上移动
					fangKuaiManager:moveShang()
				end
			end
		end
	end
    return true
end

function onGameLayerTouchEnded(x, y)
	--方块是否正在被移动,初始化
	fangKuaiManager.isMoveing = false
	--点击结束,计算融合
	fangKuaiManager:touthEndFunC()
	--单个方块的是否正在被移动.初始化
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			fk.moveing = false
		end
	end

	cclog("----------------华丽的分割线----------------")
    return true
end