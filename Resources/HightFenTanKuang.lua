HightFenTanKuang=Class(...,function(obj,agr)
                      if agr and type(agr) == 'table' then
                           for i,v in pairs(agr) do
                              obj[i] = v
                           end
                      end
                 end )

function HightFenTanKuang:createLayer()
  	self.layer = CCBuilderReaderLoad("FKHightTanKuang.ccbi",CCBProxy:create(),self)
  	self.layer = tolua.cast(self.layer,"CCLayer")
  	self.layer:setPosition(ccp(0,0))
  	scene:addChild(self.layer)
  	hightFenTanKuang = self

  	self:init()
end

function HightFenTanKuang:deleteSelf()
    if self.fangxiang == "left" then
        local move = CCMoveTo:create(0.5, ccp(-winSize.width , winSize.height*0.5))
        hightFenTanKuang.hightFen:runAction(move)
    end

    if self.fangxiang == "right" then
        local move = CCMoveTo:create(0.5, ccp(winSize.width*2 , winSize.height*0.5))
        hightFenTanKuang.hightFen:runAction(move)
    end

    local function funC( ... )
        gameLayer.liZiNode:removeAllChildrenWithCleanup(true)
        hightFenTanKuang.layer:removeFromParentAndCleanup(true)
        gameManager.jiaoDianCeng = "GameLayer" 
        hightFenTanKuang = nil
    end
    GF_performWithDelay(0.5,funC)
end

function HightFenTanKuang:init()
	--self.mAnimationManager:runAnimationsForSequenceNamed("in")
  	gameManager.jiaoDianCeng = "HightFenTanKuang"
  	self.ttf:setString(gameManager.topFKFen)
  	self.text:setString(Localizable.FKJiLu)

  	local weiShu = math.floor(math.log10(gameManager.topFKFen) + 1)
	if weiShu == 1 then
		self.ttf:setFontSize(140)
	elseif weiShu == 2 then
		self.ttf:setFontSize(88)
	elseif weiShu == 3 then
		self.ttf:setFontSize(55)
	elseif weiShu == 4 then
		self.ttf:setFontSize(44)
	else
		self.ttf:setFontSize(33)
	end
end
