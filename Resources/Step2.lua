function XinShouManager:jianCe_step2()
	local function jianCe( ... )
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				if fk.fenShu == 6 then
					return true
				end
			end
		end
		return false
	end

	if self.bu == 1 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep2_1)
	elseif self.bu == 2 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep2_2)
		fangKuaiManager.nextFenShu = 3
		fangKuaiManager:nextFangKuaiIn()
	elseif jianCe() then
		--完成了
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep2_3)
		self.step = self.step + 1
		self.bu = 0
	end
end