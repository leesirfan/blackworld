function XinShouManager:jianCe_step3()
	local function jianCe( ... )
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				if fk.fenShu == 12 then
					return true
				end
			end
		end
		return false
	end

	if self.bu == 1 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep3_1)
		fangKuaiManager.nextFenShu = 3
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 4 then
		fangKuaiManager.nextFenShu = 3
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 5 then
		fangKuaiManager.nextFenShu = 3
		fangKuaiManager:nextFangKuaiIn()	
	end

	if jianCe() then
		--完成了
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep3_2)
		self.step = self.step + 1
		self.bu = 0
	end
end