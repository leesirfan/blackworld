--点击结束后调用,用来计算方块的融合
function FangKuaiManager:touthEndFunC()
	--手指移动结束,游戏执行中,touth锁定
	gameManager.jiaoDianCeng = ""
	--把需要移动的方块进行移动,并且在容器内变换位置
	fangKuaiManager:touthEndMove()
	--把没有移动拉伸的方块恢复
	fangKuaiManager:weiDongLaShenHuiFu()

	--查看本轮操作是否已经产生了下一个方块
	function yanChiDiaoYong()
		if isXSMoShi then
			if fangKuaiManager.isTouthYes then
				playEffect("whiff.wav")
				fangKuaiManager.isTouthYes = false
				xinShouManager:jianCe_step()
			end

			gameLayer.layer:setTouchEnabled(true)
		else
			if fangKuaiManager.isTouthYes then
				playEffect("whiff.wav")
				fangKuaiManager.isTouthYes = false
				fangKuaiManager:nextFangKuai()
			end
		end

		--可以继续游戏,解除touth锁定
		gameManager.jiaoDianCeng = "GameLayer"
	end
	GF_performWithDelay(fangKuaiManager.huaDongTime,yanChiDiaoYong)
end

--把没有移动拉伸的方块恢复
function FangKuaiManager:weiDongLaShenHuiFu()
	for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			if fk.moveing == false and fk.fenShu > 0 then
				local move = CCEaseExponentialOut:create(CCMoveTo:create(fangKuaiManager.huaDongTime, fk.beginPos))
				fk.sp:runAction(move)
				local scale = CCEaseExponentialOut:create(CCScaleTo:create(fangKuaiManager.huaDongTime, 1,1))
				fk.sp:runAction(scale)
			end
		end
	end
end

--把需要移动的方块进行移动,并且在容器内变换位置
function FangKuaiManager:touthEndMove()
	local fangXiang = fangKuaiManager.moveFangXiang
	if fangXiang == "shang" then
		for i=HANG,1,-1 do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				--首先判断方块是否被移动,没动的不参与计算
				if fk.moveing then
					local nowPosX,nowPosY = fk.sp:getPosition()
					--判断是否移动到移动达到的跨度
					if nowPosY - fk.beginPos.y > spSize.height*RONGHEBI then
						--达到了移动所需的跨度,进行移动
						local moveTime = (fk.beginPos.y + spSize.height - nowPosY) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, ccp(nowPosX , fk.beginPos.y+spSize.height)))
						--移动后修改信息
						fk.beginPos = ccp(nowPosX , fk.beginPos.y+spSize.height)
						local fk1 = fangKuaiManager.fangKuaiTable[i+1][j]
						local function funC()
							fangKuaiManager:fangKuaiRongHeYanChi(fk)
						end
						fangKuaiManager:fangKuaiRongHe(fk,fk1)

						local arr = CCArray:create()
						arr:addObject(move)
						arr:addObject(CCCallFunc:create(funC))
						fk.sp:runAction( CCSequence:create(arr))
						--需要生成新的方块
						fangKuaiManager.isTouthYes = true
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangPuMan(fk)
					else
						--没有达到移动所需的跨度,回归原处
						local moveTime = (nowPosY - fk.beginPos.y) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, fk.beginPos))
						fk.sp:runAction(move)
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangSuoXiao(fk)
					end
				end
			end
		end
	end

	if fangXiang == "xia" then
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				--首先判断方块是否被移动,没动的不参与计算
				if fk.moveing then
					local nowPosX,nowPosY = fk.sp:getPosition()
					--判断是否移动到移动达到的跨度
					if fk.beginPos.y - nowPosY > spSize.height*RONGHEBI then
						--达到了移动所需的跨度,进行移动
						local moveTime = ( nowPosY  - (fk.beginPos.y - spSize.height)) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, ccp(nowPosX , fk.beginPos.y-spSize.height)))
						--移动后修改信息
						fk.beginPos = ccp(nowPosX , fk.beginPos.y-spSize.height)
						local fk1 = fangKuaiManager.fangKuaiTable[i-1][j]
						local function funC()
							fangKuaiManager:fangKuaiRongHeYanChi(fk)
						end
						fangKuaiManager:fangKuaiRongHe(fk,fk1)
						
						local arr = CCArray:create()
						arr:addObject(move)
						arr:addObject(CCCallFunc:create(funC))
						fk.sp:runAction( CCSequence:create(arr))

						--需要生成新的方块
						fangKuaiManager.isTouthYes = true
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangPuMan(fk)
					else
						--没有达到移动所需的跨度,回归原处
						local moveTime = (fk.beginPos.y - nowPosY) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, fk.beginPos))
						fk.sp:runAction(move)
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangSuoXiao(fk)
					end
				end
			end
		end
	end

	if fangXiang == "left" then
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				--首先判断方块是否被移动,没动的不参与计算
				if fk.moveing then
					local nowPosX,nowPosY = fk.sp:getPosition()
					--判断是否移动到移动达到的跨度
					if fk.beginPos.x - nowPosX > spSize.width*RONGHEBI then
						--达到了移动所需的跨度,进行移动
						local moveTime = ( nowPosX  - (fk.beginPos.x-spSize.width)) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, ccp(fk.beginPos.x-spSize.width , nowPosY)))
						--移动后修改信息
						fk.beginPos = ccp(fk.beginPos.x-spSize.width , nowPosY)
						local fk1 = fangKuaiManager.fangKuaiTable[i][j-1]
						local function funC()
							fangKuaiManager:fangKuaiRongHeYanChi(fk)
						end
						fangKuaiManager:fangKuaiRongHe(fk,fk1)
						
						local arr = CCArray:create()
						arr:addObject(move)
						arr:addObject(CCCallFunc:create(funC))
						fk.sp:runAction( CCSequence:create(arr))

						--需要生成新的方块
						fangKuaiManager.isTouthYes = true
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangPuMan(fk)
					else
						--没有达到移动所需的跨度,回归原处
						local moveTime = (fk.beginPos.x - nowPosX) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, fk.beginPos))
						fk.sp:runAction(move)
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangSuoXiao(fk)
					end
				end
			end
		end
	end

	if fangXiang == "right" then
		for i=1,HANG do
			for j=LIE,1,-1 do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				--首先判断方块是否被移动,没动的不参与计算
				if fk.moveing then
					local nowPosX,nowPosY = fk.sp:getPosition()
					--判断是否移动到移动达到的跨度
					if nowPosX - fk.beginPos.x > spSize.width*RONGHEBI then
						--达到了移动所需的跨度,进行移动
						local moveTime = (fk.beginPos.x + spSize.width - nowPosX) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, ccp(fk.beginPos.x+spSize.width , nowPosY)))
						--移动后修改信息
						fk.beginPos = ccp(fk.beginPos.x+spSize.width , nowPosY)
						local fk1 = fangKuaiManager.fangKuaiTable[i][j+1]
						local function funC()
							fangKuaiManager:fangKuaiRongHeYanChi(fk)
						end
						fangKuaiManager:fangKuaiRongHe(fk,fk1)

						
						local arr = CCArray:create()
						arr:addObject(move)
						arr:addObject(CCCallFunc:create(funC))
						fk.sp:runAction( CCSequence:create(arr))

						--需要生成新的方块
						fangKuaiManager.isTouthYes = true
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangPuMan(fk)
					else
						--没有达到移动所需的跨度,回归原处
						local moveTime = (nowPosX - fk.beginPos.x) / XIAOZHUNRONGHESPEED
						fangKuaiManager.huaDongTime = moveTime
						local move = CCEaseExponentialOut:create(CCMoveTo:create(moveTime, fk.beginPos))
						fk.sp:runAction(move)
						--白色遮挡效果处理
						fangKuaiManager:baiSeKuangSuoXiao(fk)
					end
				end
			end
		end
	end
end

--传入2个方块,第一个方块吞并第二个方块,此方法会立即执行
function FangKuaiManager:fangKuaiRongHe(fk,fk1)
	local tempX = fk.X
	local tempY = fk.Y
	--判断目标方块是否为空方块
	if fk1.fenShu == 0 then
		--目标方块是空方块,互换位置
		fk.X = fk1.X
		fk.Y = fk1.Y
		fangKuaiManager.fangKuaiTable[fk.X][fk.Y] = fk
		fk1.X = tempX
		fk1.Y = tempY
		fangKuaiManager.fangKuaiTable[fk1.X][fk1.Y] = fk1
	else
		--可以融合,把被融合赋值到融合的身上,播放动画后会自动删除
		fk.rongHeFK = fk1
		--把移动的方块赋值到被清除方块的位置
		fk.X = fk1.X
		fk.Y = fk1.Y
		fangKuaiManager.fangKuaiTable[fk.X][fk.Y] = fk
		--移动方块原来的位置出生成新的空白方砖
		fangKuaiManager.fangKuaiTable[tempX][tempY] = FangKuai()
		fangKuaiManager.fangKuaiTable[tempX][tempY]:new(tempX,tempY)
		--目标处有方块,首先计算积分
		if fk.fenShu == 1 or fk.fenShu == 2 then
			--无论是1分还是2分,合成后肯定是三分
			fk.fenShu = 3
		else
			--发现是白色方块,直接分数*2
			fk.fenShu = fk.fenShu * 2
			--判断是否成为本盘游戏的最高分
			if fk.fenShu > fangKuaiManager.topFen then
				fangKuaiManager.topFen = fk.fenShu
			end
		end
	end
end

--传入2个方块,第一个方块吞并第二个方块,此方法会延迟执行,等待动画
function FangKuaiManager:fangKuaiRongHeYanChi(fk)
	--被清除的方块,精灵删除
	if fk.rongHeFK ~= nil then
		fk.rongHeFK:deleteSelf()
		fk.rongHeFK = nil
		--根据积分更新方块的纹理
		local funC = function()
			fk:updateSp()
		end
		
		local fangXiang = fangKuaiManager.moveFangXiang
		local sc1 = CCScaleTo:create(1/6,0,1)
		local sc2 = CCScaleTo:create(1/6,1,1)
		local arr = CCArray:create()
		arr:addObject(sc1)
		arr:addObject(CCCallFunc:create(funC))
		arr:addObject(sc2)
		fk.sp:runAction( CCSequence:create(arr))
	end
end
