require "lua/extern"
require "lua/data/SkillItemInfo"


SkillConfig = {}
SkillConfig.__index = SkillConfig

function SkillConfig:loadData()

    local skillContent = CCString:createWithContentsOfFile(CCFileUtils:sharedFileUtils():fullPathForFilename("games/skill.json")):getCString()
    local jsonData = cjson.decode(skillContent)
    local dataValue = jsonData.data

    local miNumbers = table.getn(dataValue)
    
    for i = 1, miNumbers ,1 do
        local skillInfoData = dataValue[i]
        
        local skillItemInfo = SkillItemInfo.create() 
        skillItemInfo.id = skillInfoData.id
        skillItemInfo.name = skillInfoData.name
        skillItemInfo.desc = skillInfoData.desc
        
        --设置对象
        self.skills[i] = skillItemInfo
    end
end

function SkillConfig.create()
    local skillCfg = {}          
    setmetatable(skillCfg,SkillConfig) 
    
    skillCfg.skills={} 
    return skillCfg
end

function getSkillById(skillId)
    local skillItemCount = table.getn(self.skills)
    local skillItemRet = nil
    
    if skillItemCount > 0 then
        for i=1, skillItemCount, 1 do
            local skillItemTest =self.skills[i]

            if skillItemTest.id == skillId then
                skillItemRet = skillItemTest
                break
            end
        end
    end
    return skillItemRet
end
















