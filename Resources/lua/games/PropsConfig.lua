require "lua/extern"
require "lua/data/PropItemInfo"


PropsConfig = {}
PropsConfig.__index = PropsConfig

function PropsConfig:loadData()

    local propContent = CCString:createWithContentsOfFile(CCFileUtils:sharedFileUtils():fullPathForFilename("games/item.json")):getCString()

    local jsonData = cjson.decode(propContent)
    local dataValue = jsonData.data

    local miNumbers = table.getn(dataValue)
    
    for i = 1, miNumbers ,1 do
        local propInfoData = dataValue[i]
        
        local propItemInfo = PropItemInfo.create() 
        propItemInfo.bodyType = propInfoData.bodyType
        propItemInfo.name = propInfoData.name
        propItemInfo.baseQuality = propInfoData.baseQuality
        propItemInfo.description = propInfoData.description
        propItemInfo.id = propInfoData.id
        propItemInfo.icon = propInfoData.icon
        propItemInfo.maxexp = propInfoData.maxexp
        propItemInfo.growTemp = propInfoData.growTemp
        propItemInfo.maxexp = propInfoData.maxexp
        propItemInfo.comprice= propInfoData.comprice
        propItemInfo.compound = propInfoData.compound
        
        --设置对象
        self.propItems[i] = propItemInfo
    end
    

end

function PropsConfig.create()
    local propsCfg = {}          
    setmetatable(propsCfg,PropsConfig) 
    
    propsCfg.propItems={} 
    return propsCfg
end

function PropsConfig:getPropItemInfoById(propId)
    local propItemCount = table.getn(self.propItems)
    local propItemRet = nil
    
    if propItemCount > 0 then
        for i=1, propItemCount, 1 do
            local propItemTest =self.propItems[i]

            if propItemTest.id == propId then
                propItemRet = propItemTest
                break
            end
        end
    end
    return propItemRet
end















