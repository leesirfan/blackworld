require "lua/extern"

MonsterItemInfo = {}
MonsterItemInfo.__index = MonsterItemInfo

function MonsterItemInfo.create()
    local monsterItem = {}          
    setmetatable(monsterItem,MonsterItemInfo) 
    
    monsterItem.id = nil
    monsterItem.nickname = nil
    
    return monsterItem
end