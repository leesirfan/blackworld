require "lua/extern"

SkillItemInfo = {}
SkillItemInfo.__index = SkillItemInfo

function SkillItemInfo.create()
    local skillItem = {}          
    setmetatable(skillItem,SkillItemInfo) 
    
    skillItem.id = nil
    skillItem.name = nil
    skillItem.desc = nil

    return skillItem
end