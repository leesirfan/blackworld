require "lua/extern"

MapItemInfo = {}
MapItemInfo.__index = MapItemInfo

function MapItemInfo.create()
    local mapItem = {}          
    setmetatable(mapItem,MapItemInfo) 
    
    mapItem.nickname = nil 
    mapItem.desc = nil
    mapItem.enemy = nil 
    mapItem.enemydesc = nil 
    
    mapItem.bid = nil 
    mapItem.mapId = nil 
    mapItem.itemId = nil 
    mapItem.exp = nil
    mapItem.coin = nil

    return mapItem
end