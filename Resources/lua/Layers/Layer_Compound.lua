require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerCompound = LayerCompound or {}
ccb["LayerCompound"] = LayerCompound

local layer = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)

    return true
end

function comfirmEmployLayer(frId)
    
    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_compound.ccbi",proxy,LayerConfirmEmploy)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, -129, true);
    
    local btnItemLeft = tolua.cast(LayerCompound["btnItemLeft"],"CCControlButton")
    local btnItemRight = tolua.cast(LayerCompound["btnItemRight"],"CCControlButton")
    local btnItemTop = tolua.cast(LayerCompound["btnItemTop"],"CCControlButton")
    local btnItemBottom = tolua.cast(LayerCompound["btnItemBottom"],"CCControlButton")
    local btnClose = tolua.cast(LayerCompound["btnClose"],"CCControlButton")

    btnItemLeft:setTouchPriority(-130)
    btnItemRight:setTouchPriority(-130)
    btnItemTop:setTouchPriority(-130)
    btnItemBottom:setTouchPriority(-130)
    btnClose:setTouchPriority(-130)
    return layer
end

local function clickAtCloseButton()
   layer:removeFromParentAndCleanup(true)
end


local function clickedAtLeftItem()

end

local function clickedAtRightItem()

end

local function clickedAtTopItem()

end

local function clickedAtBottomItem()

end


LayerCompound["clickAtCloseButton"] = clickAtCloseButton
LayerCompound["clickedAtLeftItem"] = clickedAtLeftItem
LayerCompound["clickedAtRightItem"] = clickedAtRightItem
LayerCompound["clickedAtTopItem"] = clickedAtTopItem
LayerCompound["clickedAtBottomItem"] = clickedAtBottomItem








