require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/Layers/Layer_SelectRole"
require "lua/network/NetInfo"
require "lua/objects/CardCharactor"

local scheduler = CCDirector:sharedDirector():getScheduler()

LayerLogin = LayerLogin or {}
ccb["LayerLogin"] = LayerLogin

local editName
local editPwd

local layer = nil
local schedule_handleMessages_id = nil

--处理101
local function handleMsg101()
local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),101)
        local msgCount = msgRet:count()
        if msgCount > 0 then
            --移除Loading界面
            LoadingLayer.dismiss()
            
            local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
            msgRet:removeObjectAtIndex(0)
            msgCount = msgRet:count()
            cclog("datalength = "..ioMsg:getData())
            
            local jsonData = cjson.decode(ioMsg:getData())
            
            local bLoginResult = jsonData.result
            if bLoginResult then
                local resultData = jsonData.data
                local userId = resultData.userId
                local characterId = resultData.characterId
                local bHasRole = resultData.hasRole
                
                UserInfo.userId = userId
                UserInfo.characterId = characterId
                UserInfo.hasRole = bHasRole

                if bHasRole then
                    --角色存在,加载角色信息,跳到主界面
                    local loginPanel = tolua.cast(LayerLogin["loginPanel"],"CCSprite")  
                    local btnLogin = tolua.cast(LayerLogin["btnLogin"],"CCControlButton")  
                    loginPanel:removeFromParentAndCleanup(true)
                    btnLogin:removeFromParentAndCleanup(true)
                    
                    --发送103请求玩家数据消息
                    local tabRoleInfo ={}
                    tabRoleInfo["userId"] = UserInfo.userId
                    tabRoleInfo["characterId"] = UserInfo.characterId

                    --构造json
                    --发送获取玩家信息消息
                    local jsonData = cjson.encode(tabRoleInfo)
                    NetInfo.socketManager:sendMessage(jsonData,103)
                else
                    --角色不存在
                    local scene = CCScene:create()
                    scene:addChild(selectRoleLayer())
                    CCDirector:sharedDirector():replaceScene(scene)
                end

            end
        end

end

--处理103
local function handleMsg103()
    local msgRet = utils.getMsgsFromQuene(NetInfo.socketManager:getRecvQuene(),103)
    local msgCount = msgRet:count()
    if msgCount > 0 then
        LoadingLayer.dismiss()
        
        local ioMsg = tolua.cast( msgRet:objectAtIndex(0),"IOMessage")
        msgRet:removeObjectAtIndex(0)
        
        local jsonData = cjson.decode(ioMsg:getData())
        local bResult = jsonData.result
        local message = jsonData.message
        local data = jsonData.data
        
        if bResult then
            --处理新角色信息
            local uName = data.name
            local uPower = data.power
            local uCid = data.cid
            local uGas = data.gas
            local uLevel = data.level
            local uProfession = data.profession
            local uMaxExp = data.maxexp
            local uExp = data.exp
            local uCoin = data.coin
            local uYuanbao = data.yuanbao
            
            UserInfo.name = uName
            UserInfo.power = uPower
            UserInfo.characterId = uCid
            UserInfo.gas = uGas
            UserInfo.level = uLevel
            UserInfo.profession = uProfession
            UserInfo.maxexp = uMaxExp
            UserInfo.exp = uExp
            UserInfo.coin = uCoin
            UserInfo.yuanbao = uYuanbao
            
            local btnStart = tolua.cast(LayerLogin["btnStart"],"CCControlButton") 
            btnStart:setVisible(true)
            
        else
            cclog("handleMsg103 error")
        end

    end
end

--处理消息
local function handleMessages()
    if NetInfo.socketManager ~= nil then
        handleMsg101()
        handleMsg103()
    end
end

local function onEnter()
    schedule_handleMessages_id = scheduler:scheduleScriptFunc(handleMessages, 1, false)
end

local function onExit()
    scheduler:unscheduleScriptEntry(schedule_handleMessages_id)
end

function loginLayer()
    --设置网络

    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_login.ccbi",proxy,LayerLogin)
    layer = tolua.cast(node,"CCLayer")
    
    local nodeUserName = tolua.cast(LayerLogin["userName"],"CCNode")    
    local nodeUserPwd = tolua.cast(LayerLogin["userPwd"],"CCNode")  
    
    local nX,nY = nodeUserName:getPosition()
    local pX,pY = nodeUserPwd:getPosition()

    editName = CCEditBox:create(CCSizeMake(280,40), CCScale9Sprite:create())
    editName:setPosition(ccp(nX,nY))
        
    editPwd = CCEditBox:create(CCSizeMake(280,40), CCScale9Sprite:create())
    editPwd:setPosition(ccp(pX,pY))
    
    local loginPanel = tolua.cast(LayerLogin["loginPanel"],"CCSprite")    
    if nil ~= loginPanel then
        loginPanel:addChild(editName)
        loginPanel:addChild(editPwd)
    end
    
    --注册事件
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    layer:registerScriptHandler(onNodeEvent)

    return layer
end

local function onCCControlButtonLoginClicked()
    --弹出Loading界面
    LoadingLayer.show()

    local userName = editName:getText();
    local userPwd = editPwd:getText();
    
    -------用户信息-------
    local tabLoginInfo ={}
    tabLoginInfo["username"] = userName
    tabLoginInfo["password"] = userPwd
    
    --构造json
    local jsonData = cjson.encode(tabLoginInfo)
    NetInfo.socketManager:sendMessage(jsonData,101)
end

local function onCCControlButtonStartClicked()
    --进入home界面
    local hpLayer = homePageLayer()
    local hpScene = CCScene:create()
    hpScene:addChild(hpLayer)
    CCDirector:sharedDirector():replaceScene(hpScene)
end

LayerLogin["onCCControlButtonLoginClicked"] = onCCControlButtonLoginClicked
LayerLogin["onCCControlButtonStartClicked"] = onCCControlButtonStartClicked










