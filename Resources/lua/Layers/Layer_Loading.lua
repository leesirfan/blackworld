require "lua/utils/utils"

LoadingLayer = {}
LoadingLayer.__index = LoadingLayer

function LoadingLayer.create()
    
    local loadingLayer = {}          
    setmetatable(loadingLayer,LoadingLayer)

    local cache = CCSpriteFrameCache:sharedSpriteFrameCache()
    cache:addSpriteFramesWithFile("UI/loading.plist")
    
    local layer = CCLayer:create()
    local lodingSprite = CCSprite:createWithSpriteFrameName("load1.png")
    local animation = utils.getAnimationFromFile("animations/animation_loading.plist")
    animation:setLoops(999999)
    lodingSprite:runAction(CCAnimate:create(animation))
    
    local winSize = CCDirector:sharedDirector():getWinSize()
    lodingSprite:setPosition(ccp(winSize.width/2,winSize.height/2))
   
    layer:addChild(lodingSprite)
    
    loadingLayer.Layer = layer
    return loadingLayer
end

function LoadingLayer:getUI()
    return self.Layer 
end

function LoadingLayer.show()
    local loadingLayer = LoadingLayer.create()
    loadingLayer:getUI():setTag(100000)
    CCDirector:sharedDirector():getRunningScene():addChild(loadingLayer:getUI())
end

function LoadingLayer.dismiss()
    local loadingNode = CCDirector:sharedDirector():getRunningScene():getChildByTag(100000)
    if loadingNode ~= nil then
        loadingNode:removeFromParentAndCleanup(true)
    end
end










