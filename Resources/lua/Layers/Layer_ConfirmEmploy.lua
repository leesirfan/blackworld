require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerConfirmEmploy = LayerConfirmEmploy or {}
ccb["LayerConfirmEmploy"] = LayerConfirmEmploy

local layer = nil
local friendId = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)

    return true
end

function comfirmEmployLayer(frId)
    friendId = frId
    
    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_confirmemploy.ccbi",proxy,LayerConfirmEmploy)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, -129, true);
    
    local btnClose = tolua.cast(LayerConfirmEmploy["btnClose"],"CCControlButton")
    local btnEmploy = tolua.cast(LayerConfirmEmploy["btnEmploy"],"CCControlButton")
    
    btnClose:setTouchPriority(-130)
    btnEmploy:setTouchPriority(-130)
    
    return layer
end

local function onCCControlButtonEmployClicked()

    --弹出Loading界面
    LoadingLayer.show()

    -------角色信息-------
    local tabEmployInfo ={}
    tabEmployInfo["characterId"] = UserInfo.characterId
    tabEmployInfo["friendid"] = friendId

    --构造json
    local jsonData = cjson.encode(tabEmployInfo)
    NetInfo.socketManager:sendMessage(jsonData,2301)
end

local function clickAtCloseButton()
   layer:removeFromParentAndCleanup(true)
end

LayerConfirmEmploy["onCCControlButtonEmployClicked"] = onCCControlButtonEmployClicked
LayerConfirmEmploy["clickAtCloseButton"] = clickAtCloseButton









