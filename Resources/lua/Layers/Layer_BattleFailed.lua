require "CCBReaderLoad"
require "lua/utils/utils"
require "lua/data/UserInfo"
require "lua/Layers/Layer_Loading"
require "lua/network/NetInfo"

LayerBattleFailed = LayerBattleFailed or {}
ccb["LayerBattleFailed"] = LayerBattleFailed

local layer = nil

local function onEnter()

end

local function onExit()
    layer:unregisterScriptTouchHandler()
end

local function onTouch(event, x, y)
    return true
end

function battleFailedLayer(setData)
    local  proxy = CCBProxy:create()
    local  node  = CCBuilderReaderLoad("CCBLayers/layer_battlefailed.ccbi",proxy,LayerBattleFailed)
    layer = tolua.cast(node,"CCLayer")
    
    --触摸屏蔽
    local function onNodeEvent(event)
        if event == "enter" then
            onEnter()
        elseif event == "exit" then
            onExit()
        end
    end
    
    local labelHonner = tolua.cast(LayerBattleFailed["labelHonner"],"CCLabelTTF")
    local labelGod = tolua.cast(LayerBattleFailed["labelGod"],"CCLabelTTF")
    local labelVigor = tolua.cast(LayerBattleFailed["labelVigor"],"CCLabelTTF")

    local btnBackMap = tolua.cast(LayerBattleFailed["btnBackMap"],"CCControlButton")
    btnBackMap:setTouchPriority(-128)
    
    labelHonner:setString(setData.exp)
    labelGod:setString(setData.coin)
    labelVigor:setString(setData.huoli)
    
    --设置items
    local itemNum = table.getn(setData.item)
    if itemNum > 0 then
        for i=1, itemNum, 1 do 
            local itemId = setData.item[i]
            local propItemInfo = GameInfo.propConfig:getPropItemInfoById(itemId)
            local fileName = string.format("UI/qs_%04d.png",propItemInfo.icon)
            --
            local spriteItem = tolua.cast(LayerBattleFailed["spriteItem"..i],"CCSprite")
            spriteItem:setScale(0.5)
            spriteItem:initWithFile(fileName)
        end
    end
    
    --设置星级
    local starNum = setData.star
    local spriteStar1 = tolua.cast(LayerBattleFailed["spriteStar1"],"CCSprite")
    local spriteStar2 = tolua.cast(LayerBattleFailed["spriteStar2"],"CCSprite")
    local spriteStar3 = tolua.cast(LayerBattleFailed["spriteStar3"],"CCSprite")
    if starNum == 1 then 
        spriteStar1:setVisible(true)
    elseif starNum == 2 then 
        spriteStar1:setVisible(true)
        spriteStar2:setVisible(true)
    elseif starNum == 3 then 
        spriteStar1:setVisible(true)
        spriteStar2:setVisible(true)
        spriteStar3:setVisible(true)
    end

    layer:setTouchEnabled(true)
    layer:registerScriptTouchHandler(onTouch, false, 0, true)

    return layerend
end
    --返回地图
local function clickedAtBackButton()
    cclog("you failed")
end

LayerBattleFailed["clickedAtBackButton"] = clickedAtBackButton







