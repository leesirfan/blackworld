GameManager=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function GameManager:new()
	gameManager = self
    --这个变量决定游戏的当前焦点层
    self.jiaoDianCeng = ""
    --当前游戏局是否结束
    self.isGameing = false
    --储存历史最高方块的积分
    self.topFKFen = 12
    --储存历史最高的总积分
    self.topFen = 0
end

function GameManager:chuCunGame()
    --rank记录
    fangKuaiManager:endJuLu()
    --把本局游戏加入到存档列表
    local benJuChuCun = {}
    local random = math.ceil(math.random() * 99999 + 1)

    benJuChuCun[1] = fangKuaiManager.jieTuName
    benJuChuCun[2] = fangKuaiManager.endGameJiFen
    benJuChuCun[3] = fangKuaiManager.endGameName

    table.insert(tableChuCun,benJuChuCun)

    --根据积分,对存档进行排序
    if table.getn(tableChuCun) >= 2 then
        function sortFunc (a, b)
            return a[2] > b[2]
        end
        table.sort(tableChuCun,sortFunc)
    end

    --检测记录是否大于了10个,如果是,则删除1个
    if table.getn(tableChuCun) > JILUBAOCUNSHULIANG then
        os.remove(CCFileUtils:sharedFileUtils():getWritablePath()..tableChuCun[table.getn(tableChuCun)][1])
        table.remove(tableChuCun)
    end

    --储存文件
    GF_saveTableToFile(tableChuCun,"tableChuCun",CCFileUtils:sharedFileUtils():getWritablePath().."tableChuCun.lua")

    --本局游戏结束
    self.isGameing = false
end

--刷新最高单个方块积分记录
function GameManager:shuaXinZuiGaoFKFen()
    for i=1,4 do
        local particle = CCParticleSystemQuad:create(i..".plist")
        --particle:setAutoRemoveOnFinish(true)
        gameLayer.liZiNode:addChild(particle)
    end

end

--刷新最高总积分记录
function GameManager:shuaXinZuiGaoFen()
    for i=1,4 do
        local particle = CCParticleSystemQuad:create(i..".plist")
        --particle:setAutoRemoveOnFinish(true)
        gameLayer.liZiNode:addChild(particle)
    end
end