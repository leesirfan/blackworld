HallOfRecLayer=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )
local function getTableCount(tab)
    local count = 0;

    if tab then
        for c,v in pairs(tab) do
            count = count+1;
        end
    end

    return count;
end

local function moveBack(t,point)
    -- 直接返回远点
    local move = CCMoveTo:create(0.5,point);
    t:runAction(move);
end

-- 上下移动动画结束动作
function HallOfRecLayer:endMove()
    local moveLyaerHeight = self.moveLayer:getContentSize().height;
    -- 判断移动层是否大于整个屏幕
    if moveLyaerHeight > winSize.height*0.8 then
        local _y = self.moveLayer:getPositionY();
        if _y > 0 then
            -- 返回远点
            moveBack(self.moveLayer,ccp(0,0));
        else
            if (_y+ moveLyaerHeight)<winSize.height*0.8 then
                moveBack(self.moveLayer,ccp(0,winSize.height*0.8-moveLyaerHeight));
            end
        end
    else
        moveBack(self.moveLayer,ccp(0,winSize.height*0.8-moveLyaerHeight));
    end
end

function HallOfRecLayer:createLayer(i)
	--记录下当前记录的排名
    self.paiMing = i
    --把父节点装进容器,用来拖动
    table.insert(beginManager.tuoDongNode,self)
	cclog("历史积分层 - 被创建")

	self.layer = CCBuilderReaderLoad("HallOfRecLayer.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	
	hallOfRecLayer = self
	beginLayer.layer:addChild(self.layer)
	self:registerTouch(self.layer);

    local scrollView = CCScrollView:create()
    --   显示显示的区域
    scrollView:setViewSize(CCSizeMake(winSize.width, winSize.height*0.8));--设置view的大小
    --scrollView:setContentOffset(ccp(0,0));

    --continerLayer->setContentSize(CCSizeMake(960, 320));--设置滚动区域的大小
    --  显示滑动的区域大小 scrollview的实际大小
    scrollView:setContentSize(CCSizeMake(winSize.width, winSize.height*0.8));--设置scrollview区域的大小

    --因为要自己实现触摸消息，所以这里设为false//设置需要滚动的内容
    scrollView:setTouchEnabled(false);

    local height = 0.68;
    -- 创建一个移动层
    self.moveLayer = CCLayer:create()--CCLayerColor:create(ccc4(0, 128, 128, 255), 300, 300);
    self.moveLayer:ignoreAnchorPointForPosition(false);
    self.moveLayer:setAnchorPoint(CCPointMake(0, 0))
    self.moveLayer:setPosition(ccp(0,0));

    -- 先取最高分
    local tempSp = ChallengeSp:createSp();
    -- 获取挑战的记录信息
    local rankTemp = rank[1];
    local count = getTableCount(rankTemp);
    local moveLayerHeight = tempSp:getContentSize().height*1.95*(count+1);
    self.moveLayer:setContentSize(CCSizeMake(winSize.width,moveLayerHeight));
    self.moveLayer:setPosition(ccp(0,(scrollView:getContentSize().height-moveLayerHeight)));
    -- 把最高分添加到movelayer曾
    local point = self.moveLayer:convertToNodeSpace(ccp(winSize.width/2,winSize.height*height))
    tempSp:setPosition(point);
    -- 显示分数跟名字
    tempSp:setObj(Localizable.BeginLayer_High,rank[2]);
    height = height-0.15;
    self.moveLayer:addChild(tempSp);
    
    --对所有键进行排序  
    local function sortFunc(a,b)
        return a[1]>b[1] 
    end
    table.sort(rankTemp,sortFunc);

    --GF_dump(rankTemp);

    table.foreach(rankTemp, function(i, v) 
        local sp = ChallengeSp:createSp();
        local point = self.moveLayer:convertToNodeSpace(ccp(winSize.width/2,winSize.height*height))
        sp:setPosition(point);
        -- 显示分数跟名字
        sp:setObj(Localizable.BeginLayer_Most_1..v[1]..Localizable.BeginLayer_Most_2,v[2]);
        height = height-0.15;
        self.moveLayer:addChild(sp);

        --检查是否是历史最高分
        if v[1] > gameManager.topFKFen then
            gameManager.topFKFen = v[1]
        end
    end)

    scrollView:addChild(self.moveLayer);
	self.layer:addChild(scrollView);

    --历史最高总分赋值
    gameManager.topFen = rank[2]
end

function HallOfRecLayer:registerTouch(t)
		-- 判断是否点击了这个层
		self.checkClick = false;
		self.pointTemp = null;
	 	local function onTouchBegan(x, y)
            if self.layer:boundingBox():containsPoint(ccp(x,y)) then
            	self.checkClick = true;
            	self.pointTemp = ccp(x,y);
                -- 表示左右上下都不能拖动
                self.isMoveTop = 0;
            	return true
            else
            	self.checkClick = false;
            	return false;
            end
        end

        local function onTouchMoved(x, y)

            if self.checkClick and self.pointTemp then
                -- 判断是纵向移动还是横向移动
                if beginManager.isMoveTop ~= 2 then
                    if math.abs(y-self.pointTemp.y)>2 or math.abs(x-self.pointTemp.x)>2  then
                        if  math.abs(y-self.pointTemp.y)>math.abs(x-self.pointTemp.x) then
                            beginManager.isMoveTop = 2;
                        else
                            beginManager.isMoveTop = 1;
                            -- 不在进这里判断了
                            self.checkClick = false;
                            return;
                        end
                    end
                end
                -- 手势上下移动的距离比左右移动的距离大的时候执行下面的操作
                if beginManager.isMoveTop then
                	local tempPoint = ccp(self.moveLayer:getPositionX(),self.moveLayer:getPositionY()+(y-self.pointTemp.y));
                	self.pointTemp.y=y;
                	self.moveLayer:setPosition(tempPoint);
                end
            end
        end

        local function onTouchEnded(x, y)
            self.checkClick = false;
            self.pointTemp = null;
            --beginManager.isMoveTop = 1;
            -- 判断是否超出移动范围了
            self:endMove();
        end

        local function onTouch(eventType, x, y)
            if eventType == "began" then   
                return onTouchBegan(x, y)
            elseif eventType == "moved" then
                return onTouchMoved(x, y)
            else
                return onTouchEnded(x, y)
            end
        end
	     -- 设置touch事件  
		t:registerScriptTouchHandler(onTouch,false,-1,false)
		t:setTouchEnabled(true)

end

function HallOfRecLayer:deleteSelf()
	cclog("历史积分层 - 被删除")
	hallOfRecLayer.layer:removeFromParentAndCleanup(true)
	hallOfRecLayer = nil
end