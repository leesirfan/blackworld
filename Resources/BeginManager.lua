BeginManager=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                         for i,v in pairs(agr) do
                            obj[i] = v
                         end
                    end
                end )

function BeginManager:new()
	beginManager = self
	--这个属性用来控制移动时是否已经判断出移动方向
	self.isMoveing = false
	--这个集合里装着所有可以被拖动的父节点
	self.tuoDongNode = {}
	--这里迭代器记录着哪个框在屏幕的最中央
	self.zhong = BEGINLAYERTINGZHIKONGJIAN
	--拖动菜单框的宽度,用来绝对每个组件的距离
	self.kuangSize = nil
    -- 判断挑战页面是否向上移动 一共有3种状态，0表示谁都不能拖，1表示可以左右拖，2表示只能上下拖
	self.isMoveTop = 1;
end