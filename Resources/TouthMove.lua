function FangKuaiManager:muBiaoDuiBi(fk,fk1)
	--判断目标方块是否存在
	if fk1.fenShu ~= 0 then
		--目标方块存在,判断他是否已经被移动了
		if fk1.moveing == false then
			--目标方块没有被移走,判断当前方块是否是白色
			if fk.fenShu >=3 then
				--当前是白色方块,检测目标方块是否和现在的一样
				if fk.fenShu == fk1.fenShu then
					fk.moveing = true
				end
			else
				--当前不是白色方块,检测是否和目标方块合并
				if fk.fenShu == 1 and fk1.fenShu == 2 or fk.fenShu == 2 and fk1.fenShu == 1 then
					--我的天啊,竟然可以合并,合体吧
					fk.moveing = true
				end
			end

			--修改遮挡关系
			gameLayer.kuangZeroNode:reorderChild(fk.layer,2)
			gameLayer.kuangZeroNode:reorderChild(fk1.layer,1)
		else
			--目标方块被移动了,我们也移动吧
			fk.moveing = true
		end
	else
		--目标方块不存在,直接移动吧
		fk.moveing = true
	end
end

--向下拖动时运行的方法
function FangKuaiManager:moveShang()
	fangKuaiManager.moveFangXiang = "shang"
  	for i=HANG,1,-1 do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			--判断此方块是否上面一行还有东西
			if fk.X < HANG and fk.fenShu > 0 then
				local fk1 = fangKuaiManager.fangKuaiTable[i+1][j]
				--进行对比,查看当前方块是否可以被移动
				fangKuaiManager:muBiaoDuiBi(fk,fk1)
			end
		end
	end
end

--向上拖动时运行的方法
function FangKuaiManager:moveXia()
    fangKuaiManager.moveFangXiang = "xia"
    for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			--判断此方块是否下面一行还有东西
			if fk.X > 1 and fk.fenShu > 0 then
				local fk1 = fangKuaiManager.fangKuaiTable[i-1][j]
				--进行对比,查看当前方块是否可以被移动
				fangKuaiManager:muBiaoDuiBi(fk,fk1)
			end
		end
	end
end

--向左拖动时运行的方法
function FangKuaiManager:moveLeft()
    fangKuaiManager.moveFangXiang = "left"
    for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			--判断此方块是否左边一列还有东西
			if fk.Y > 1 and fk.fenShu > 0 then
				local fk1 = fangKuaiManager.fangKuaiTable[i][j-1]
				--进行对比,查看当前方块是否可以被移动
				fangKuaiManager:muBiaoDuiBi(fk,fk1)
			end
		end
	end
end

--向右拖动时运行的方法
function FangKuaiManager:moveRight()
    fangKuaiManager.moveFangXiang = "right"
    for i=1,HANG do
		for j=LIE,1,-1 do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			--判断此方块是否左边一列还有东西
			if fk.Y < LIE and fk.fenShu > 0 then
				local fk1 = fangKuaiManager.fangKuaiTable[i][j+1]
				--进行对比,查看当前方块是否可以被移动
				fangKuaiManager:muBiaoDuiBi(fk,fk1)
			end
		end
	end
end

--拖动时移动的函数
function FangKuaiManager:moveing()
	--是否已经挤压过其他方块
	local isJiYa = false
    for i=1,HANG do
		for j=1,LIE do
			local fk = fangKuaiManager.fangKuaiTable[i][j]
			local nowPosX,nowPosY

			--判断方块是否可以被移动
			if fk.moveing then
				--判断当前移动方向
				local fangXiang = fangKuaiManager.moveFangXiang
				nowPosX,nowPosY = fk.sp:getPosition()

				--当前正在向上移动
				if fangXiang == "shang" then
					--判断个现在正在上移还是下移
					if moveChangDu.y < 0 then
						--正在向上移动,判断是否会移动超出
						if nowPosY - moveChangDu.y - fk.beginPos.y >= spSize.height then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(ccp(nowPosX , fk.beginPos.y+spSize.height))
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX , nowPosY-moveChangDu.y))
						end
					else
						--正在向下移动,判断是否会移动超出
						if nowPosY - moveChangDu.y - fk.beginPos.y <= 0 then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(fk.beginPos)
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX , nowPosY-moveChangDu.y))
						end
					end
				end
				
				if fangXiang == "xia" then
					--判断个现在正在上移还是下移
					if moveChangDu.y > 0 then
						--正在向下移动,判断是否会移动超出
						if fk.beginPos.y - nowPosY + moveChangDu.y <= spSize.height then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(ccp(nowPosX , nowPosY-moveChangDu.y))
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX , fk.beginPos.y-spSize.height))
						end
					else
						--正在向上移动,判断是否会移动超出
						if nowPosY - moveChangDu.y - fk.beginPos.y >= 0 then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(fk.beginPos)
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX , nowPosY-moveChangDu.y))
						end
					end
				end
			
				--当前正在向左移动
				if fangXiang == "left" then
					--判断个现在正在左移还是右移
					if moveChangDu.x > 0 then
						--正在向左移动,判断是否会移动超出
						if fk.beginPos.x - nowPosX + moveChangDu.x >= spSize.width then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(ccp(fk.beginPos.x-spSize.width , nowPosY))
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX-moveChangDu.x , nowPosY))
						end
					else
						--正在向上移动,判断是否会移动超出
						if fk.beginPos.x - nowPosX + moveChangDu.x <= 0 then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(fk.beginPos)
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX - moveChangDu.x,nowPosY))
						end
					end
				end

				--当前在正向右移动
				if fangXiang == "right" then
					--判断个现在正在左移还是右移
					if moveChangDu.x < 0 then
						--正在向右移动,判断是否会移动超出
						if nowPosX - moveChangDu.x - fk.beginPos.x >= spSize.width then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(ccp(fk.beginPos.x+spSize.width , nowPosY))
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX-moveChangDu.x , nowPosY))
						end
					else
						--正在向左移动,判断是否会移动超出
						if fk.beginPos.x - nowPosX + moveChangDu.x >= 0 then
							--到了最远距离,直接设置到最远距离
							fk.sp:setPosition(fk.beginPos)
						else
							--没有到最远距离,正常移动
							fk.sp:setPosition(ccp(nowPosX - moveChangDu.x,nowPosY))
						end
					end
				end

				--根据重叠,对重叠进行处理
				fangKuaiManager:baiKuangLaShen(fk)
				--根据重叠,对未移动的方块进行微量偏移
				if isJiYa == false then
					FangKuaiManager:weiDongPianYi(fk)
					isJiYa = true
				end
			end
		end
	end
end