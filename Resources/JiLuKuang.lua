JiLuKuang=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )

function JiLuKuang:createLayer(i)
    --记录下当前记录的排名
    self.paiMing = i + JILUYOUBIANDEKONGJIANSHU
    --把父节点装进容器,用来拖动
    table.insert(beginManager.tuoDongNode,self)
	--cclog("记录框%d - 被创建",self.paiMing)
	self.layer = CCBuilderReaderLoad("jiLuKuang.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
    if beginManager.kuangSize == nil then
        beginManager.kuangSize = self.kuang:getContentSize()
    end
	self.layer:setPosition(ccp(0 - (self.paiMing - 1 ) * beginManager.kuangSize.width  * 1.15
        - winSize.width*JILUYOUBIANDEKONGJIANSHU,0))
	beginLayer.layer:addChild(self.layer)
end

function JiLuKuang:deleteSelf()
	--cclog("记录框%d - 被删除",self.paiMing)
	self.layer:removeFromParentAndCleanup(true)
end

function JiLuKuang:init()
	local tempTable = tableChuCun[self.paiMing - JILUYOUBIANDEKONGJIANSHU]

    local jiLuTu = CCSprite:create(CCFileUtils:sharedFileUtils():getWritablePath()..tempTable[1])
    local posX,posY = self.middleNode:getPosition()
    jiLuTu:setPosition(ccp(posX,posY))
    local scale = self.kuang:getContentSize().width / jiLuTu:getContentSize().width
    jiLuTu:setScale(scale)
    self.layer:addChild(jiLuTu)

    self.jifenTTF:setString(tempTable[2])
    self.name:setString(tempTable[3])
end