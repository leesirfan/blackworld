
-- 当前系统使用的语言
GV_languageType = CCApplication:sharedApplication():getCurrentLanguage()
GV_LanguageTypeEnum = {
     kLanguageEnglish = 0,
    kLanguageChinese = 1,
    kLanguageFrench = 2,
    kLanguageItalian = 3,
    kLanguageGerman = 4,
    kLanguageSpanish = 5,
    kLanguageRussian = 6,
}
