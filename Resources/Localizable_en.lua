Localizable = {
	XSMoShiStep0_1 = "Threes!\nMeet 1 & 2",
	XSMoShiStep0_2 = "Swipe anywhere;\nmove everybody.",
	XSMoShiStep0_3 = "Rearrange numbers by\npushing 'em into walls.",
	XSMoShiStep0_4 = "Use the walls to add\n1 & 2 together.",

	XSMoShiStep1_1 = "Auspicious!",

	XSMoShiStep2_1 = "New numbers appear\nwhen you move cards!",
	XSMoShiStep2_2 = "3 + 3 = 6",
	XSMoShiStep2_3 = "You're getting it!",

	XSMoShiStep3_1 = "Numbers 3 and higher\nwill only add together\nif they are twins",
	XSMoShiStep3_2 = "Fantastic!",

	XSMoShiStep4_1 = "1 will ONLY add with 2",
	XSMoShiStep4_2 = "2 will ONLY add with 1",
	XSMoShiStep4_3 = "Make a 24!",
	XSMoShiStep4_4 = "This is THREES",
	XSMoShiStep4_5 = "make the highest\nnumber you can!",

	BeginLayer_retry = "Start",
	BeginLayer_back = "back",
	BeginLayer_Option = "Game Options",
	BeginLayer_Music = "Music",
	BeginLayer_Effects = "Sound Effects",
	BeginLayer_High = "High Score",
	BeginLayer_Most_1 = "Most ",
	BeginLayer_Most_2 = "'s on a finished board",

	FKJiLu = "恭喜你获得了\n新的记录"
}