function XinShouManager:jianCe_step4()
	local function jianCe( ... )
		for i=1,HANG do
			for j=1,LIE do
				local fk = fangKuaiManager.fangKuaiTable[i][j]
				if fk.fenShu == 24 then
					return true
				end
			end
		end
		return false
	end

	if self.bu == 1 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep4_1)
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 2 then
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 3 then
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 4 then
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 5 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep4_2)
		fangKuaiManager.nextFenShu = 2
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 6 then
		fangKuaiManager.nextFenShu = 2
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 7 then
		fangKuaiManager.nextFenShu = 2
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 9 then
		fangKuaiManager.nextFenShu = 2
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 10 then
		fangKuaiManager.nextFenShu = 2
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 11 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep4_3)
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 12 then
		fangKuaiManager.nextFenShu = 1
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 13 then
		fangKuaiManager.nextFenShu = 3
		fangKuaiManager:nextFangKuaiIn()
	elseif self.bu == 15 then
		self.bu = 12
	elseif self.bu == 20 then
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep4_5)
	elseif self.bu == 21 then
		self.step = 5
	end

	if self.bu < 16 and jianCe() then
		--完成了
		gameLayer.XSShuoMing:setString(Localizable.XSMoShiStep4_4)
		self.bu = 19
	end
end