OptinsLayer=Class(...,function(obj,agr)
                    if agr and type(agr) == 'table' then
                        for i,v in pairs(agr) do
                            obj[i] = v
                        end
                    end
                end )

function OptinsLayer:createLayer(i)
	--记录下当前记录的排名
    self.paiMing = i
    --把父节点装进容器,用来拖动
    table.insert(beginManager.tuoDongNode,self)
	cclog("设置层 - 被创建")
	self.layer = CCBuilderReaderLoad("Options.ccbi",CCBProxy:create(),self)
	self.layer = tolua.cast(self.layer,"CCLayer")
	self.layer:setPosition(ccp(0 - (self.paiMing - 1) * winSize.width,0))
	optinsLayer = self
	beginLayer.layer:addChild(self.layer)

	-- on,off 精灵
	self.enableTexture=CCTextureCache:sharedTextureCache():addImage("on.png");
    self.disableTexture=CCTextureCache:sharedTextureCache():addImage("off.png");

    -- 初始化按钮图片
    self:changeMusicImg(shengYin.isPlayBackMusic);
    optinsLayer:changeEffectsImg(shengYin.isPlayEffect);
	--初始化游戏场景
  	
  	--多语言适配
    self.titleTTF:setString(Localizable.BeginLayer_Option)
    self.musicTTF:setString(Localizable.BeginLayer_Music)
    self.effectsTTF:setString(Localizable.BeginLayer_Effects)

end
-- 修改音效按钮图片
function OptinsLayer:changeEffectsImg(check)
	self.effectSp:setTexture(((check and self.enableTexture) or self.disableTexture));
	-- 修改btn
    self.effectBtn:setBackgroundSpriteFrameForState(((check and CCSprite:create("yinXiaoOn.png"):displayFrame()) or CCSprite:create("yinXiaoOff.png"):displayFrame()), 1)
end

-- 修改背景音乐按钮图片
function OptinsLayer:changeMusicImg(check)
	self.musicSp:setTexture(((check and self.enableTexture) or self.disableTexture));
	-- 修改btn
    self.musicBtn:setBackgroundSpriteFrameForState(((check and CCSprite:create("yinYueOn.png"):displayFrame()) or CCSprite:create("yinYueOff.png"):displayFrame()), 1)
end

-- 关闭背景音乐
function OptinsLayer:music()
	-- 设置背景音乐并且返回true 或 false
	local check = settingPlayBackMusic();
	self:changeMusicImg(check);
end

-- 关闭音效
function OptinsLayer:effect()
	-- 设置背景音乐并且返回true 或 false
	local check = settingPlayEffect();
	self:changeEffectsImg(check);
end




function OptinsLayer:deleteSelf()
	cclog("设置层 - 被删除")
	optinsLayer.layer:removeFromParentAndCleanup(true)
	optinsLayer = nil
end